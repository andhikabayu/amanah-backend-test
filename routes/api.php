<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/barang/store', 'BarangController@store');
Route::get('/barang', 'BarangController@index');
Route::post('/barang/update/{id}', 'BarangController@update');
Route::post('/barang/delete/{id}', 'BarangController@destroy');
Route::post('/barang/purchase', 'BarangController@purchase');
