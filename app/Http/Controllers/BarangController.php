<?php

namespace App\Http\Controllers;
use App\Barang;

use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function index()
    {   
        $barang = Barang::all();
        return response()->json(['status'=> '200', 'message'=> 'get data success.', 'data'=> $barang]);
    }

    public function store(Request $request)
    {
        $req = $request->all();

        $barang = new Barang;
        $barang->nama = $req['nama'];
        $barang->harga = $req['harga'];
        $barang->stok = $req['stok'];

        try {
            if ($barang->save()) {
                return response()->json(['status'=> '200', 'message'=> 'store data success.', 'data'=> $barang]);   
            }

            return response()->json(['status'=> '400', 'message'=> 'Bad Request.', 'data'=> '{}']);
        } catch (\Throwable $th) {
            return response()->json(['status'=> '400', 'message'=> 'Bad Request.', 'data'=> '{}']);
        }
        
        
    }

    public function update(Request $request, $id)
    {
        $req = $request->all();

        try {
            $barang = Barang::find($id);
            $barang->nama = $req['nama'];
            $barang->harga = $req['harga'];
            $barang->stok = $req['stok'];

            if ($barang->save()) {
                return response()->json(['status'=> '200', 'message'=> 'update data success.', 'data'=> $barang]);   
            }

            return response()->json(['status'=> '400', 'message'=> 'Bad Request.', 'data'=> '{}']);
        } catch (\Throwable $th) {
            return response()->json(['status'=> '400', 'message'=> 'Bad Request.', 'data'=> '{}']);
        }
    }

    public function destroy($id)
    {
        try {
            $barang = Barang::find($id);

            if ($barang->delete()) {
                return response()->json(['status'=> '200', 'message'=> 'delete data success.', 'data'=> '{}']);   
            }
            
            return response()->json(['status'=> '400', 'message'=> 'Bad Request.', 'data'=> '{}']);
        } catch (\Throwable $th) {
            return response()->json(['status'=> '400', 'message'=> 'Bad Request.', 'data'=> '{}']);
        }
    }

    public function purchase(Request $request)
    {
        $req = $request->all();
        try {
            $barang = Barang::find($req['id_barang']);
            $barang->stok = $barang['stok'] + $req['purchase'];

            if ($barang->save()) {
                return response()->json(['status'=> '200', 'message'=> 'purchase barang with id '.$barang['id'].' success.', 'data'=> $barang]);
            }
            
            return response()->json(['status'=> '400', 'message'=> 'Bad Request.', 'data'=> '{}']);
        } catch (\Throwable $th) {
            return response()->json(['status'=> '400', 'message'=> 'Bad Request.', 'data'=> '{}']);
        }

        return response()->json(['status'=> '200', 'message'=> 'store data success.', 'data'=> $barang]);
    }
}
